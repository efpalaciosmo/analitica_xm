import cv2 as cv
import math
import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt
from skimage.transform import hough_line, hough_line_peaks


def img_read(filename: str, mode: str = 'color'):
    if mode == 'color':
        return cv.cvtColor(cv.imread(filename), cv.COLOR_BGR2RGB)
    elif mode == 'gray':
        return cv.imread(filename, cv.IMREAD_GRAYSCALE)
    else:
        return None


def img_scale(img, value_range=[0, 255]):
    return (value_range[1] - value_range[0])*(img - np.min(img))/(np.max(img)-np.min(img))+value_range[0]


def degree2minutes(degree: int) -> int:
    """
    function that receive degrees as parameters
    and returns its value in minutes
    """
    minutes = int(degree/6)
    return minutes


def degree2hour(degree: int, minutes: int) -> int:
    """
    function that receive degrees as parameters
    and returns its value in minutes
    """
    hour = int(math.fmod((360*degree-(minutes/2)), 30))
    return hour


def digitalTime(h: int, m: int,) -> str:
	time = ""
	hour = ""
	minute = ""
	if(h<10):
		hour = "{}H-:".format(h)
	else:
		hour = "{}H-:".format(h)
	if(m<10):
		minute = "{}M:".format(m)
	else:
		minute = "{}M:".format(m)
	time = hour+minute
	return time


def norm(x: int, y: int) -> float:
    return math.sqrt(math.pow(x, 2)+math.pow(y, 2))


def show_image(image):
    blurred = cv.bilateralFilter(image,11,125,255, 0)
    result = cv.Canny(blurred, threshold1=0, threshold2=255)

    tested_angles = np.linspace(-np.pi/2, np.pi/2, 360, endpoint=False)
    h, theta, d = hough_line(result, theta=tested_angles)

    fig, axes = plt.subplots(1, 3, figsize=(10, 6))
    ax = axes.ravel()

    ax[0].imshow(image, cmap=cm.gray)
    ax[0].set_title('Input image')
    ax[0].set_axis_off()
    angle_step = 0.5 * np.diff(theta).mean()
    d_step = 0.5 * np.diff(d).mean()
    bounds = [np.rad2deg(theta[0] - angle_step),
              np.rad2deg(theta[-1] + angle_step),
              d[-1] + d_step, d[0] - d_step]

    ax[1].imshow(np.log(1 + h), extent=bounds, cmap=cm.gray, aspect=1 / 1.5)
    ax[1].set_title('Hough transform')
    ax[1].set_xlabel('Angles (degrees)')
    ax[1].set_ylabel('Distance (pixels)')
    ax[1].axis('image')

    ax[2].imshow(image, cmap=cm.gray)
    ax[2].set_ylim((image.shape[0], 0))
    ax[2].set_axis_off()
    ax[2].set_title('Detected lines')

    data = []
    hour = 0
    minutes = 0
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, threshold=0.5, num_peaks=2)):
        (x0, y0) = dist * np.array([np.cos(angle), np.sin(angle)])
        ax[2].axline((x0, y0), slope=np.tan(angle + np.pi/2))
        data.append(int(np.rad2deg(angle)))
    
    minutes = degree2minutes(data[0])
    hour = degree2hour(data[1], minutes)
    plt.tight_layout()
    plt.show()
    return (degree2minutes(data[0]), degree2hour(minutes, data[1]))


def get_angles_right(image):
    # blurred = cv.GaussianBlur(image, (5, 5), 1)
    result = cv.Canny(image, threshold1=180, threshold2=255)
    tested_angles = np.linspace(-np.pi/2, np.pi/2, 360, endpoint=False)
    h, theta, d = hough_line(result, theta=tested_angles)
    data = []
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, num_peaks=2)):
        (x0, y0) = dist * np.array([np.cos(angle), np.sin(angle)])
        data.append(np.round(np.rad2deg(angle), 2))

    bestH, bestTheta, bestD = hough_line_peaks(h, theta, d, num_peaks=2)
    return data


def get_angles_left(image):
    # blurred = cv.GaussianBlur(image, (5, 5), 1)
    result = cv.Canny(image, threshold1=180, threshold2=255)
    tested_angles = np.linspace(0, np.pi, 180, endpoint=False)
    h, theta, d = hough_line(result, theta=tested_angles)
    data = []
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, num_peaks=2)):
        (x0, y0) = dist * np.array([np.cos(angle), np.sin(angle)])
        if x0 < 0 and y0 < 0:
            data.append(180-np.round(np.rad2deg(angle), 2))
        elif x0 > 0 and y0 < 0:
            data.append(180-np.round(np.rad2deg(angle), 2))

    bestH, bestTheta, bestD = hough_line_peaks(h, theta, d, num_peaks=2)
    return data
