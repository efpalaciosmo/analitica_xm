## Objetivo
Dado un conjunto de imagenes de un reloj, se pide mostrar la hora en formato digital (HH:MM) para cada imagen de
reloj analógico.

## Herramientas usadas
Se tenia la posibilidad de hacer la prueba usando lenguajes como Python, Go o C++, 
las razones por las que decidi hacerlo en Python y no en uno de los otros dos lenguajes son
las siguientes

- **Familiaridad** Aunque ya habia usado cada uno de estos lenguajes, por lejos python es con el que mas he estado practicando. C++ hace un buen rato no lo uso y hacerlo en este momento implicaria gastar mucho tiempo recordado como se hacian las cosas.

- **Toolkit** Python es un lenguaje que se a adoptado ampliamente en la industria de la vision artificial, provee de las suficientes librerias para realizar el proyecto. C++ tambien tiene dichas librerias, pero nuevamente, el tiempo es un factor necesario. En este punto es donde decidi eliminar a Go, este si lo estoy usando mas constantemente, pero las herramientas que poseen estan lejos de lo deseado para procesamiento de imagenes.

- **Eficiencia** Desde el inicio tenia claro que iba a usar tecnicas clasicas de vision artificial para intentar resolver el problema, aplicar algunos filtros, operaciones puntuales en las imagenes o por vecindad, tal vez una derivida  convulucion, en todo caso, ninguna de estas operaciones para un dataset como el que se tiene nos haria extranar las bondades de C++.

## Probar el codigo
usar los siguientes pasos si se desea usar un ambiente virtual
```shell
python -m venv .env
source .env/bin/activate (unix like systems)
pip install -r requirements.txt
python ./main.py
```