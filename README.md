
## Objetivo

Dado un conjunto de imágenes en las cuales se encuentra un reloj analógico un , se pide mostrar la hora en formato digital (HH:MM) para cada una de las imágenes.

**Herramientas usadas**
Se tenía la posibilidad de hacer la prueba usando lenguajes como Python, Go o C++, las razones por las que decidí hacerlo en Python y no en uno de los otros dos lenguajes son las siguientes

**Familiaridad**: Aunque ya había usado cada uno de estos lenguajes, por lejos python es con el que más he estado practicando. C++ hace un buen rato no lo uso y hacerlo en este momento implicaría gastar mucho tiempo recordando cómo se hacían las cosas.


**Toolkit**: Python es un lenguaje que se ha adoptado ampliamente en la industria de la visión artificial, provee de las suficientes librerías para realizar el proyecto. C++ también tiene dichas librerías, pero nuevamente, el tiempo es un factor necesario. En este punto es donde decidí eliminar a Go, éste si lo estoy usando más constantemente, pero las herramientas que poseen están lejos de lo deseado para procesamiento de imágenes.


**Eficiencia**: Desde el inicio tenía claro que iba a usar técnicas clásicas de visión artificial para intentar resolver el problema, aplicar algunos filtros, operaciones puntuales en las imágenes o por vecindad, tal vez una derivada convolución, en todo caso, ninguna de estas operaciones para un dataset como el que se tiene nos haría extrañar las bondades de C++.

